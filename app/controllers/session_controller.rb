class SessionController < ApplicationController
	def new
		
	end
	# POST
	def create
		user = Rank.authenticate(params[:usrname], params[:passwd])
		if user
			session[:user_id] = user.id
			redirect_to root_url, :notice => "Logged in!"
			# flash.now.alert = "Invalid usrname or password"
		else
			flash.now.alert = "Invalid usrname or password"
			render "new"
		end

=begin
		user = Rank.find_by_email(params[:email])
		if user && user.authenticate(params[:password])
			if params[:remember_me]
				cookies.permanent[:auth_token] = user.auth_token
			else
				cookies[:auth_token] = user.auth_token
			end
			redirect_to root_url, :notice => "Logged in!"
		else
			flash.now.alert = "Invalid email or password"
			render "new"
		end
=end

	end

	def destroy
		session[:user_id] = nil	
		cookies.delete(:auth_token)
		redirect_to root_url, :notice => "Logged out!"
	end
end
