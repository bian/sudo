class OnlineusrController < ApplicationController
	# POST
	def getlist
		#del expired record
		Onlinelist.delete_expired_usr
		#insert or alternate
		if current_user
			newusr = Onlinelist.where(:rank_id => current_user.id).limit(1)[0]
			if not newusr
				newusr = Onlinelist.new
			end
			newusr.progress = params[:prog]
			newusr.sudoku_id = session[:sudoid]
			newusr.rank_id = current_user.id
			newusr.touch
			#if (not params[:prog].blank?) and (not params[:sudoid].blank?)#avoid get method
				newusr.save
			#end
		end

		@datalist = Onlinelist.where(:sudoku_id => session[:sudoid]).select("rank_id, progress").order("progress desc")
		
		render :layout => false
  		
	end
end
