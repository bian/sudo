class UsercfgController < ApplicationController
	# post
	def savecfg
		if session[:user_id]
			@usr = Rank.find(session[:user_id])
			if @usr
				@usr.savedsudo = params[:savedsudo]#use "s" to split id and solution
				if @usr.save
					render :inline => "ok"
				end
			end
		end
	end
	# get
	def loadcfg
		if session[:user_id]
			@usr = Rank.find(session[:user_id])
			if @usr
				respond_to do |format|
					format.html { render :layout => false  }
					format.json { head :no_content }
				end
				return
			end
		end
		respond_to do |format|
			format.html { render :inline => "", :layout => false }
			format.json { head :no_content }
		end
	end
end
