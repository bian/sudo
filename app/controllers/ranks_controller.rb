# -*- coding: utf-8 -*-

class RanksController < ApplicationController
	# GET /ranks
	# GET /ranks.json
	def index
		@ranks = Rank.order("num desc").limit(50)

		@sudo_today = Sudodb.sudo_today_solved
		@usr_active_week = Sudodb.active_usrs
		@sudolatest = Sudodb.sudo_new

		session[:sudoid] = @sudolatest.id
		
		respond_to do |format|
			format.html # index.html.erb
			format.json { render json: @ranks }
		end
	end

	# GET /ranks/1
	# GET /ranks/1.json
	def show
		@rank = Rank.find(params[:id])

		respond_to do |format|
			format.html # show.html.erb
			format.json { render json: @rank }
		end
	end

	# GET /ranks/new
	# GET /ranks/new.json
	def new
		@rank = Rank.new
		respond_to do |format|
			format.html # new.html.erb
			format.json { render json: @rank }
		end
	end

	# GET /ranks/1/edit
	def edit
		@rank = Rank.find(params[:id])
	end


	# POST /ranks
	# POST /ranks.json
	def create 
		if ( params[:rank][:usrname].blank? or params[:rank][:email].blank? or params[:rank][:passwd].blank? or params[:rank][:password_confirmation].blank? )
			flash.now.alert = "usrname  or email can't be empty"
		elsif params[:rank][:passwd]!=params[:rank][:password_confirmation]
			flash.now.alert = "password not same"
		else
			@rank = Rank.new
			@rank.usrname = params[:rank][:usrname]
			@rank.passwd = params[:rank][:passwd]
			@rank.email = params[:rank][:email]
			@rank.num = 0
			if @rank.save
				session[:user_id] = @rank.id
				# 
				respond_to do |format|

					format.html { redirect_to root_url, :notice => "Signed up!" }
					format.json { render json: @rank.errors, status: :unprocessable_entity }
				end
				return
			end
		end
		respond_to do |format|

			format.html { render "new"}
			format.json { render json: @rank.errors, status: :unprocessable_entity }
		end
	end

	# PUT /ranks/1
	# PUT /ranks/1.json
	def update
		@rank = Rank.find(params[:id])

		respond_to do |format|
			if @rank.update_attributes(params[:rank])
				format.html { redirect_to @rank, notice: 'Rank was successfully updated.' }
				format.json { head :no_content }
			else
				format.html { render action: "edit" }
				format.json { render json: @rank.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /ranks/1
	# DELETE /ranks/1.json
	def destroy
		@rank = Rank.find(params[:id])
		@rank.destroy

		respond_to do |format|
			format.html { redirect_to ranks_url }
			format.json { head :no_content }
		end
	end
	# POST
	def check
		@msg =  ""
		@sudolinerecv=  params[:sudoall]
		@sudolatest = Sudodb.last
		@sudolatest_shrink = ""
		if @sudolatest
			@sudolatest_shrink = @sudolatest.line.gsub(";","").gsub("0","").gsub(" ","").gsub("\n","")
			if @sudolatest.rank.nil?
				if params[:sudoall]== @sudolatest_shrink
					@usr = Rank.find(session[:user_id])
					if @usr
						# @sudolatest.solvedby = @usr.usrname
						@sudolatest.rank = @usr
						@usr.num += 1
						Rank.transaction  do
							if @sudolatest.save!
								@msg = "<p>我们已经保存了你的新记录</p>"
							else
								@msg ="<strong>Warning</strong>: Saved user message failed in sudoku table"
							end
							if @usr.save!
								@msg += "<p>这是你应得的奖励: <strong>战绩 +1 </strong></p><p>你的个人战绩已经更新了: <strong>"+@usr.usrname+": " + @usr.num.to_s+"</strong></p>"

							else
								@msg += "<strong>Error: </strong>Saving your personal data failed, your data will not change</p>"
							end
						end
					else
						@msg = "<strong>Warning</strong>: no user register message found!session may be expired!!"
					end
				else
					@msg = "<strong>Warning</strong>: your sudoku was not coincidence with the one we give you"
				end
			else
				@msg = "<strong>Warning</strong>: Sorry, this sudoku has been solved by someone else before you done."
			end
		else
			@msg = "<strong>Warning</strong>: no sudoku generated, how you did this?"

		end
		render :layout => false

	end

	#private

	def is_authened
		session[:user_id] = @current_user.id
		Rank.find(session[:user_id])
	end

end
