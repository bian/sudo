# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/


                                
$ = jQuery

current_progress = 0
emptycellnum = 0

getfixedsudo = ->
        strline = ""
        for ki in [0..80]
                if $("#c"+ki).hasClass("fixed")
                        strline += $("#c"+ki ).val()
        return strline

resetcellcolor = ->
        for ki in [0..60]
                if ((ki == 0) or (ki == 6) or (ki == 30) or (ki == 54) or (ki == 60))
                        bakcol = "#A1CFC2"
                        for kii in [0..2]
                                for kjj in [0..2]
                                        $("#c"+(9*kjj+kii+ki)).attr("style","background-color:"+bakcol)    
                if ((ki == 3) or (ki == 27) or (ki == 33) or (ki == 57))
                        bakcol = "#BFDEBF"
                        for kii in [0..2]
                                for kjj in [0..2]
                                        $("#c"+(9*kjj+kii+ki)).attr("style","background-color:"+ bakcol)    

markrow = (leftindex) ->
        for ki in [leftindex..(leftindex+8)]
                $("#c"+ki).attr("style","background-color:#ffaaaa")    
markcol = (topindex) ->
        for ki in [0..8]
                $("#c"+(ki*9+topindex)).attr("style","background-color:#ffaaaa")    
marksquare = (lefttopindex) ->
        for ki in [0..2]
                for kj in [0..2]
                        $("#c"+(lefttopindex + ki*9 + kj)).attr("style","background-color:#ffaaaa")
checkvalid = (cell) ->
        #console.log "checking inputed number valid.."
        id = cell.id
        value = cell.value
        if not value
                #console.log "empty string"
                return false

        index = parseInt(id.slice(1))
        colindex = index%9
        rowindex = Math.floor(index/9)
        #console.log "row,col",rowindex,colindex
        #console.log  "checking in row.."
        for ki in [0..8]
                if ki != colindex
                        if $("#c"+(ki+index-colindex) ).val() == value
                                return false

        #console.log  "check column.."
        for ki in [0..8]
                if ki != rowindex
                        #console.log ki*9+colindex,$("#c"+(ki*9+colindex) ).val()
                        if $("#c"+(ki*9+colindex) ).val() == value
                                return false
        # check small square
        #    +---------------------------------------------+
        #    |             c                               |
        #    |                                             |
        #    |                                             |
        #  r |                                             |
        #    |                                             |
        #    |                                             |
        #    +---------------------------------------------+
        bigrow = if (rowindex < 3) then 1 else (if (rowindex < 6) then 2 else 3)
        bigcol = if (colindex < 3) then 1 else (if (colindex < 6) then 2 else 3)
        #console.log "bigr,bigc",bigrow,bigcol
        #console.log "checking square.."
        for ki in [( (bigrow-1)*3)..(bigrow*3-1)]
                for kj in [((bigcol-1)*3)..(bigcol*3-1)]
                        #console.log ki,kj
                        if not ( ki == rowindex and kj == colindex )
                                if $("#c"+(ki*9 + kj)).val() == value
                                        return false
        return true

checkrow = (leftindex) ->
        li = [0,0,0,0,0,0,0,0,0]
        for ki in [0..8]
                vl = $("#c"+(ki+leftindex)).val()
                if vl
                        li[vl-1] += 1
        for ki in [0..8]
                if li[ki] > 1
                        return false
        true
checkcol = (topindex) ->
        li = [0,0,0,0,0,0,0,0,0]
        for ki in [0..8]
                vl = $("#c"+(ki*9+topindex)).val()
                if vl
                        li[vl-1] += 1
        for ki in [0..8]
                if li[ki] > 1
                        return false
        true
checksquare = (lefttopindex) ->
        li = [0,0,0,0,0,0,0,0,0]
        for ki in [0..2]
                for kj in [0..2]
                        vl =  $("#c"+(lefttopindex + ki*9 + kj)).val()
                        if vl
                                li[vl-1] += 1
        for ki in [0..8]
                if li[ki] > 1
                        return false
        true        
checkall = ->
        #console.log "check all..."
        resetcellcolor()
        pass = true
        for ki in [0..8]
                if not checkrow(ki*9)
                        markrow(ki*9)
                        pass = false
        for ki in [0..8]
                if not checkcol(ki)
                        markcol(ki)
                        pass = false
        for ki in [0..2]
                for kj in [0..2]
                        lefttopindex= ki*27+kj*3
                        if not checksquare(lefttopindex)
                                marksquare(lefttopindex)
                                pass = false
        return pass                                        
           
isfulled = ->
        #console.log "checking is fullfilled.."
        isfull = true
        validnum = 0
        for ki in [0..80]
                if $("#c"+ki).hasClass("blank")
                        vl = $("#c"+ki).val()
                        if not (vl and (vl > '0') and (vl <= '9'))
                                isfull = false
                        else
                                validnum += 1
        current_progress = validnum
        return isfull
        #cl "test"
showreward = ->
        #console.log 'show reward'
        $(".rewardbox").show()
        if $(".ajresp").length>0
                strline = getfixedsudo()
                #console.log "strline",strline
                $.ajax '/check',
                        type : 'POST'
                        data :
                                sudoall : strline
                                #usrname : 'value'
                        dataType: 'html'
                        success  : (res, status, xhr) ->
                                
                                #console.log 'ajax:success'
                        error    : (xhr, status, err) ->
                                #console.log 'ajax:error'
                                $(".ajresp").html("error ocurred!")
                        complete : (xhr, status) ->
                                #console.log 'ajax:complete'
                                $(".ajresp").html("#{xhr.responseText}")

clearall = ->
        #console.log 'clear all boxes..'
        resetcellcolor()
        for ki in [0..80]
                if $("#c"+ki).hasClass("blank")
                        $("#c"+ki).val("")
        current_progress = 0
btsave = ->
        if $(".ajresp").length>0
                $(".btsave").html("Saving...")                
                strline = ""
                usrcfg = ""
                for ki in [0..80]
                        vv = $("#c"+ki ).val()
                        if $("#c"+ki).hasClass("fixed")
                                strline += vv
                        else
                                if vv.length>0 
                                        usrcfg += vv
                                else
                                        usrcfg += "0"
                #console.log usrcfg
                $.ajax 'usercfg/savecfg',
                        type : 'POST'
                        data :
                                savedsudo : strline + "s" +usrcfg
                                #usrname : 'value'
                        dataType: 'html'
                        success  : (res, status, xhr) ->
                                #console.log 'ajax:success'
                                if xhr.responseText == "ok"
                                        $(".btsave").html("Save")
                        error    : (xhr, status, err) ->
                                #console.log 'ajax:error'
                                alert "error occured"
                                $(".btsave").html("Save :(")
                        complete : (xhr, status) ->
                                #console.log 'ajax:complete'
                                $(".btsave").html("Save")
                                
        else
                alert "只有注册用户才有存储空间，请先注册或登录"
        
btload = ->
        if $(".ajresp").length>0
                $(".btload").html("Loading...")                
                strline = getfixedsudo()
                #console.log "strline",strline
                $.ajax 'usercfg/loadcfg',
                        type : 'GET'
                        dataType: 'html'
                        success  : (res, status, xhr) ->
                                #console.log 'ajax:success'
                                $(".btload").html("Load")
                                if xhr.responseText
                                        if xhr.responseText.split("s")[0] != strline
                                                #console.log xhr.responseText.split("s")[0],strline
                                                return
                                        res = xhr.responseText.split("s")[1]
                                        #console.log res
                                        kj = 0
                                        for ki in [0..80]
                                                #console.log "vv",vv
                                                if $("#c"+ki).hasClass("blank")
                                                        if kj >= res.length
                                                                return
                                                        vv = res[kj]
                                                        #console.log ki,kj
                                                        kj += 1
                                                        if vv == "0"
                                                                $("#c"+ki ).val("")
                                                        else
                                                                $("#c"+ki ).val(vv)
                                        checkall()
                        error    : (xhr, status, err) ->
                                #console.log 'ajax:error'
                                alert "error occured"
                                $(".btload").html("Load :(")
                        complete : (xhr, status) ->
                                #console.log 'ajax:complete'
                                $(".btload").html("Load")
        else
                alert "只有注册用户才有存储空间，请先注册或登录"

onscale = ->
        if $(".btscale").html() == "[+]"
                $(".btscale").html("[-]")
                $(".cl_table input").css("font-size","3.7em")
                $(".cl_table input").css("width", "70px")
                $(".cl_table input").css("height", "70px")
        else
                $(".btscale").html("[+]")
                $(".cl_table input").css("font-size","2.7em")
                $(".cl_table input").css("width","50px")
                $(".cl_table input").css("height","50px")

dsdsdsa = (data) ->
        $(".ollist").html(data)
loadonlineusrdata = ->
        # get usr progress
        $.post("/onlineusr",{prog : Math.round(100*current_progress/emptycellnum)}, ((data) => dsdsdsa(data)) )

$ ->
        #Dom content loaded
        $(".cell.blank").keydown  (event) =>
                keycode = event.which
                #console.log 'key',keycode
                # or (keycode >= 97 and keycode <= 105
                if not ((keycode >=49 and keycode <= 57) or keycode == 8 or keycode == 46 or keycode ==9 or (keycode >=97 and keycode <= 105))
                        if (keycode > 36 and keycode < 41) or keycode == 9
                                ii = parseInt(event.target.id.slice(1))
                                if (keycode == 37)#left
                                        ii--
                                else if (keycode == 38)#up
                                        ii -= 9
                                else if (keycode == 9 or keycode==39)#tab,r
                                        ii += 1
                                else if (keycode == 40)#up
                                        ii += 9
                                #move focus
                                ii += 81 if ii < 0
                                ii -= 81 if ii >80
                                $("#c"+ii).focus()
                        else
                                event.preventDefault()
                else
                        event.target.value=""
                
        $(".cell.blank").keyup  (event) =>
                #test-->
                #$("sudobox").fadeOut(3000,showreward)
                #return
                #<--end test
                #if checkvalid(event.target)
                if checkall()
                        if isfulled()
                                #sent to server check again
                                $("sudobox").fadeOut(3000,showreward)
                                #$("sudobox").hide(showreward)#fadeOut(3000,showreward)
        $(".btclear").click ->
                clearall()
        $(".btsave").click ->
                btsave()
        $(".btload").click ->
                btload()
        setInterval(loadonlineusrdata,5000)
        for ki in [0..80]
                if $("#c"+ki).hasClass("blank")
                        emptycellnum += 1
