require 'sudoku_mgr'

class Sudodb < ActiveRecord::Base
	belongs_to :rank #, :counter_cache => true 
	has_many :onlinelists
	attr_accessible :created_on, :line
	
	def self.sudo_today_solved
		where(:updated_at => (DateTime.now.utc.beginning_of_day)..DateTime.now.utc.end_of_day).order('created_at desc')#.offset(1)
	end

	def self.active_usrs
		where(:updated_at => (DateTime.now.utc.beginning_of_week)..DateTime.now.utc.end_of_day).group(:rank_id).select("rank_id, count(*) AS sudonum").order("sudonum DESC")
	end

	def self.sudo_new
		sudolast = self.last#where(:rank => nil )
		if not sudolast#.exists?
			newsudo = get_new_sudoku()
			sudolast = self.new
			sudolast.line = array_to_line(get_new_sudoku())
			sudolast.save!
		elsif not sudolast.rank.nil?
			newsudo = get_new_sudoku()
			sudolast = self.new
			sudolast.line = array_to_line(get_new_sudoku())
			sudolast.save! 
		end
		return sudolast
	end

end


