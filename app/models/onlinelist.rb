class Onlinelist < ActiveRecord::Base
  	attr_accessible :progress#, :rank_id
	has_one :rank
	belongs_to :sudodb
	validates :rank_id, :uniqueness => true
	#before_save :updatetime
	#def updatetime
		
	def self.delete_expired_usr
		where("updated_at < ?", 10.seconds.ago).delete_all
  
	end
end
