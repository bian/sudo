# -*- coding: utf-8 -*-



class Rank < ActiveRecord::Base
	has_many :sudodbs
	belongs_to :onlinelist
	attr_accessible :usrname, :num, :created_on, :updated_at, :savedsudo
	validates :usrname,  :presence => true,  
	:uniqueness => true,  	
	:length => {:minimum => 3, :maximum => 12}  

	validates :email, :presence => true,   
	:length => {:minimum => 4, :maximum => 254},  
	:uniqueness => true,  
	:format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}  

	# validates :passwd, :presence => true,   
	# :length => {:minimum => 4, :maximum => 20}

	attr_accessible :email, :passwd, :passwd_confirmation

	attr_accessor  :passwd
	before_save :encrypt_password, :modifycounts
	validates_confirmation_of :passwd
	validates_presence_of :passwd, :on => :create
	# validates_presence_of :email
	# validates_uniqueness_of :email
	#login
	def self.authenticate(usrname, passwd)
		user = where(:usrname => usrname).first
		if user && user.password_hash == BCrypt::Engine.hash_secret(passwd, user.password_salt)
			user
		else
			nil
		end
	end

	def encrypt_password
		if passwd.present?
			self.password_salt = BCrypt::Engine.generate_salt
			self.password_hash = BCrypt::Engine.hash_secret(passwd, password_salt)
		end
	end

	def modifycounts
		#can't be size
		if not self.sudodbs.blank?
			if self.id.nil?
				self.num = 0
			else
				self.num = self.sudodbs.count
			end
		else
			self.num = 0
		end
	end
	#:format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}  
	# def self.authenticate_safely_simply(usrname, passwd)
	# 	where(:usrname => usrname, :passwd => passwd).first
	# end

	#
	before_create { generate_token(:auth_token) }

	def send_password_reset
		generate_token(:password_reset_token)
		self.password_reset_sent_at = Time.zone.now
		save!
		UserMailer.password_reset(self).deliver
	end

	def generate_token(column)
		begin
			self[column] = SecureRandom.urlsafe_base64
		end while Rank.exists?(column => self[column])
	end
end
