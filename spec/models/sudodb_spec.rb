require 'spec_helper'

describe Sudodb do
	
	before(:each) do
		
	end

	context 'get today solved sudo' do
		before(:each) do
		end
		it 'should have no unsolved sudo listed' do
			Sudodb.sudo_today_solved.each do |a|
				a.rank_id.should be_blank
			end
		end
	end

	context 'get new unsolved sudo' do
		before(:each) do
			@sudo = Sudodb.sudo_new
		end
		it 'should return a valid sudoku' do
			@sudo.should_not be_blank
		end
		it 'should return an unsolved sudoku' do
			@sudo.rank_id.should be_blank
		end
	end

end
