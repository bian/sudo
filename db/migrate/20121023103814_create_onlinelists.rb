class CreateOnlinelists < ActiveRecord::Migration
  def change
    create_table :onlinelists do |t|
      t.integer :rank_id
      t.integer :sudoku_id
      t.integer :progress
      t.timestamps
    end
  end
end
