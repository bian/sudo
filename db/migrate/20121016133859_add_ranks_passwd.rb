class AddRanksPasswd < ActiveRecord::Migration
  def change
  	add_column :ranks,  :password_hash,  :string
  	add_column :ranks,  :password_salt,  :string
  end

end
