class ChangeDefault < ActiveRecord::Migration
  def change
  	change_column :ranks,  :num,  :integer, :default => 0
  end

end
