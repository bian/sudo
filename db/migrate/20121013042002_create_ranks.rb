class CreateRanks < ActiveRecord::Migration
  def change
    create_table :ranks do |t|
      t.string :usrname
      t.string :num
      t.date :created_on
      t.datetime :updated_at
      t.timestamps
    end
    
    create_table :sudodbs do |t|
      t.date :created_on
      t.string :line
      t.string :solvedby
      t.timestamps
    end

  end
end
