class AddPasswordResetToUsers < ActiveRecord::Migration
  def change
    add_column :ranks, :password_reset_token, :string
    add_column :ranks, :password_reset_sent_at, :datetime
  end
end
