# -*- coding: utf-8 -*-
#/ruby


require "sudoku"

class MySudoku < Sudoku::S3
  include Sudoku::Generator
  include Sudoku::Solver
end#class

require "sudoku_cal"

class SudokuAlex
def initialize(complication=50)
end

def self.getRandNum(dest_counts)
 which_number= rand(9)+1
 if dest_counts[which_number-1] == 0 then
   #look up
   tmp_number=-1
   if which_number!=9 then
      for ki in (which_number+1)..9
          if dest_counts[ki-1] != 0 then
             tmp_number= ki
             break
          end
      end       
   end
   if tmp_number==-1 then
      if which_number!=1 then
         for ki in 1..(which_number-1)
             if dest_counts[which_number-ki-1] != 0 then
                tmp_number= ki
                break
             end
         end
      end
   end
   which_number = tmp_number
 end
 return which_number
end#endfun

def self.get_random_sequence(n, m)
#index base 0
  if m.zero? then
  return []
  end

  s = get_random_sequence(n-1, m-1)
  t = rand(n+1)
  s.concat s.include?(t) ? [n] : [t]
  return s
  
end

def self.dig_hole_in_farray(farr,level=1)
  #level = 1,2
  dest_counts = Array.new(9,5+level)#all equal 0 is done!
  #which_number
  while true do
    which_number= getRandNum(dest_counts)
    if which_number == -1 then
      break
    end
    #get random sequence
    random_seq= get_random_sequence(8,4-level)
    count = 0
    for ki in 0..8
      for kj in 0..8
      #while dest_counts[9]!=0 do
        if farr[ki][kj] == which_number then
          if random_seq.include?(count) then
            count +=1
            next
          end
          tmp=farr[ki][kj]
          farr[ki][kj] = 0
          #check
          if not is_array_sole_answer(farr) then
            farr[ki][kj] = tmp
          #else 
            #dest_counts[which_number-1] -= 1
          end
          count +=1
        end
      end
    end

    dest_counts[which_number-1]=0
  #p dest_counts
  end
end

end#class

def d2array_to_sutxt(arr)
    sutxt = "3: "
    for ki in arr
      for kj in ki
          sutxt.concat(kj.to_s+" ")
      end
    end
    return sutxt+";"
end


def is_array_sole_answer(arr)
#2 outputs
    s= MySudoku.new
    line = d2array_to_sutxt(arr)
#puts line
    s.load line
    s.solve_naive!#solve puzzle
    return s.complete?
end

#---test---function---

def printArray(arr)
for ki in arr
  p ki
end
end
def getArrayInstance()
a= [[0,0,8,0,4,3,7,0,2],
[0,0,0,0,2,0,0,0,0],
[9,0,6,8,0,0,0,0,0],
[0,4,9,0,6,0,0,0,0],
[8,0,0,0,0,0,0,0,7],
[0,0,0,0,3,0,9,1,0],
[0,0,0,0,0,2,4,0,3],
[0,0,0,0,5,0,0,0,0],
[5,0,1,7,8,0,6,0,0]]
return a
end
def getFArrayInstance()
a=[[1,5,8,6,4,3,7,9,2],
[3,7,4,5,2,9,1,8,6],
[9,2,6,8,7,1,3,4,5],

[2,4,9,1,6,7,5,3,8],
[8,1,3,4,9,5,2,6,7],
[7,6,5,2,3,8,9,1,4],

[6,8,7,9,1,2,4,5,3],
[4,9,2,3,5,6,8,7,1],
[5,3,1,7,8,4,6,2,9]]
return a
end
def testArray()
##usage:
a=getArrayInstance()
p is_array_sole_answer(a)

end

def testDighole()
a=getFArrayInstance()
SudokuAlex::dig_hole_in_farray(a)
printArray(a)
p is_array_sole_answer(a)

end

if __FILE__ == $0
   #sudo = Sudoku.new()
  # puts sudo.generateCompletedSudoku() 
=begin
require 'sudoku'    
 include Sudoku::Generator
 include Sudoku::Grid  

s = Sudoku.new(9).make_valid
   puts s



#  file=File.new('s.txt','w')
#file.puts s.to_sutxt
#file.close

  File.open "set22.sutxt" do |f|
    f.each do |line|
      s.load line
      
      puts s
      puts "#{s.solve_naive!} ajouts #{'(valide)' if s.valid?} #{'(complet)' if s.complete?}"
      puts s
      puts '-'*25
    end
  end
end
=end

testDighole

#testArray
end
