def setmarkch(cm,markch)
	tmparr=markch
	if (cm >= 0) & (cm <= 2)
		tmparr[0]=0
		tmparr[1]=0
		tmparr[2]=0
	elsif (cm >= 3) & (cm <= 5)
		tmparr[3]=0
		tmparr[4]=0
		tmparr[5]=0
	elsif (cm >= 6) & (cm <= 8)
		tmparr[6]=0
		tmparr[7]=0
		tmparr[8]=0
	end
end

def calrand(markch)	
	re=Array.new(9)
	for i in 0..8
		re[i]=markch[i]*(i+1)
	end
	re.delete(0)
	for i in 0..re.length-1
		re[i]=re[i]-1
	end
	return re
end

def add(arr1,arr2)
	arrtmp=[1,1,1,1,1,1,1,1,1]
	for i in 0..8
		if (arr1[i]==0) | (arr2[i]==0)
			arrtmp[i]=0
		else
			arrtmp[i]=1
		end
	end
	return arrtmp
end

def assignval(arr1,arr2)
	for i in 0..8
		arr1[i]=arr2[i]
	end
end
		
#####################  MAIN  MAIN  MAIN  ######################################
def sudokufill

sudoku= Array.new(9){ Array.new(9, 0)}
mark=Array.new(9){ Array.new(9, 1)}
marktmp=Array.new(9){ Array.new(9, 1)}

sudokubackup=Array.new(9){ Array.new(9, 0)}
markbackup=Array.new(9){ Array.new(9, 1)}
mark_samerowbackup=Array.new(9){ Array.new(9, 1)}
mark_sameareabackup=Array.new(9){ Array.new(9, 1)}

for z in 0..7

	markch=Array.new(9, 1)
	mark_samerow=Array.new(9, 1)
	mark_samearea=Array.new(9, 1)
	num=z+1
	wronglinenum=nil

	for mm in 0..8
		assignval(sudokubackup[mm],sudoku[mm])
		assignval(markbackup[mm],mark[mm])
		assignval(marktmp[mm],mark[mm])
	end
	mark_samearea=[1,1,1,1,1,1,1,1,1]
	linenum=0
	while linenum<=8 do
		if wronglinenum!=nil
			linenum=linenum-1
			for m in linenum..8
				assignval(sudoku[m],sudokubackup[m])
				assignval(mark[m],markbackup[m])
			end
			for n in (linenum+1)..8
				assignval(marktmp[n],markbackup[n])
			end
			assignval(mark_samerow,mark_samerowbackup[linenum])
			assignval(mark_samearea,mark_sameareabackup[linenum])
			wronglinenum=nil
		end
		rem=linenum%3
###########################################################################
		case rem
		when 0
			assignval(mark_samerowbackup[linenum],mark_samerow)
			assignval(mark_sameareabackup[linenum],mark_samearea)
			markch=add(marktmp[linenum],mark_samerow)
			markch=add(markch,mark_samearea)	
			re=calrand(markch)
			cm=re[rand(re.length)]
			if cm==nil
				wronglinenum=linenum
				next
			end
			sudoku[linenum][cm]=num
			mark[linenum][cm]=0
			marktmp[linenum][cm]=0
			mark_samerow[cm]=0			
			setmarkch(cm,mark_samearea)			
			linenum=1+linenum
#########################################################################		
		when 1
			assignval(mark_samerowbackup[linenum],mark_samerow)
			assignval(mark_sameareabackup[linenum],mark_samearea)
			markch=add(marktmp[linenum],mark_samerow)
			markch=add(markch,mark_samearea)
			re=calrand(markch)
			cm=re[rand(re.length)]
			if cm==nil
				wronglinenum=linenum
				next
				#return nil
			end
			sudoku[linenum][cm]=num
			mark[linenum][cm]=0
			marktmp[linenum][cm]=0
			mark_samerow[cm]=0
			setmarkch(cm,mark_samearea)
			linenum=1+linenum
##############################################################################
		when 2
			assignval(mark_samerowbackup[linenum],mark_samerow)
			assignval(mark_sameareabackup[linenum],mark_samearea)
			markch=add(marktmp[linenum],mark_samerow)
			markch=add(markch,mark_samearea)
			re=calrand(markch)
			cm=re[0]
			if cm==nil
				wronglinenum=linenum
				next
			end
			sudoku[linenum][cm]=num
			mark[linenum][cm]=0
			marktmp[linenum][cm]=0
			mark_samerow[cm]=0
			mark_samearea=[1,1,1,1,1,1,1,1,1]
			linenum=1+linenum
		end
		if linenum <0
			return -1
		end
	end
end
return sudoku
end

def sudoku_cal
	counter=0
	while counter!=1 do
		res=sudokufill
		if res!=-1
			counter=1
		end
	end
	for xx in 0..8
		for yy in 0..8
			if res[xx][yy]==0
				res[xx][yy]=9
			end
		end
	end
	#1.upto(9){|iii| p res[iii-1]}
	return res
end
#sudoku_cal

if __FILE__ == $0
   p sudoku_cal

end
